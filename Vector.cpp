#include "Vector.h"
#include <iostream>




Vector::Vector(int n) //c'tor : init the vector with n - if n is smaller then 2 it will be changed to 2
{
	if (n < 2)
		n = 2;
	_capacity = n;
	_size = 0;
	_resizeFactor = n;
	_elements = new int[n];
}


Vector::~Vector() //d'tor : delete allocated memory and destroy the vector
{
	delete[] _elements;
}


int Vector::size() const
{
	return _size;
}


int Vector::capacity() const
{
	return _capacity;
}


int Vector::resizeFactor() const
{
	return _resizeFactor;
}


bool Vector::empty() const
{
	return (_size == 0 ? true : false);
}


void Vector::push_back(const int& val) //add a value to the vector
{
	int i = 0;
	if (_size > 0 && _size < _capacity) //if the vector is not empty but not full as well
	{
		_elements[_size] = val;
		_size++;
	}
	else if (_size == 0)  //if the vector is empty
	{
		_elements[0] = val;
		_size++;
	}
	else //if the vector is full
	{
		int *temp = _elements;
		_elements = new int[_capacity + _resizeFactor];
		for (i = 0; i <= _capacity + _resizeFactor;i++)
		{
			_elements[i] = temp[i];
		}
		delete[] temp;
		_elements[_size] = val;
		_size++;
		_capacity += _resizeFactor;
	}
}


int Vector::pop_back()
{
	int RetValue = 0;
	if (_size == 0)
	{
		std::cout << "error: pop from empty vector" << std::endl;
		return -9999;
	}
	else
	{
		RetValue = _elements[_size - 1];
		_elements[_size - 1] = 0;
		_size--;
		return RetValue;
	}
}


void Vector::reserve(int n)
{
	int i = 0;
	if (_capacity < n)
	{
		for (i = 0; _capacity < n; i++) //i try to find out how much do i nead to add to the vector
		{
			_capacity += _resizeFactor;
		}


		int *temp = _elements;
		_elements = new int[_capacity];
		for (i = 0; i <= _capacity; i++)
		{
			_elements[i] = temp[i];
		}
		delete[] temp;
	}
}


void Vector::resize(int n)
{
	int *temp = _elements, i = 0;

	if (n < _capacity)
	{
		_elements = new int[n];
		for (i = 0; i <= n; i++)
		{
			_elements[i] = temp[i];
		}
		delete[] temp;
	}

	else
	{
		_capacity = n;
		_elements = new int[_capacity];
		for (i = 0; i <= _capacity; i++)
		{
			_elements[i] = temp[i];
		}
		delete[] temp;
	}
}


void Vector::resize(int n, const int& val)
{
	int  Tsize = _size; //saves the size to know how many elements to change
	resize(n); //uses the normal function
	if (Tsize > _size)
	{
		for (Tsize; Tsize <= _size; Tsize++)
		{
			_elements[Tsize] = val;
		}
	}
}


void Vector::assign(int val)
{
	int i = 0;
	for (i = 0; i <= _size; i++)
	{
		_elements[i] = val;
	}
}


int& Vector::operator[](int n) const
{
	if (n > _capacity || n < 0;)
	{
		std::cout << "error: n is bigger then the vector or smaller then zero!!!" << std::endl << "returning the first value" << std::endl;
		return _elements[0];
	}
	else
	{
		return _elements[n];
	}
}